/**
 *<p>Copyright © 北京协软科技有限公司版权所有。</p>
 *类名:AlarmPieBean
 *创建人:chensq    创建时间:2019年03月02日
 */
package com.protocolsoft.alarm.bean;
/**
 * @ClassName: AlarmPieBean
 * @Description: 告警饼图返回bean
 * @author chensq
 *
 */
public class AlarmPieBean {
	/**
	 * id
	 */
	private long id;
	/**
	 * kpisName
	 */
	private String  kpisName;
	/**
	 * value
	 */
	private long value;
	/**
	 * name
	 */
	private String name;
	
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getKpisName() {
		return kpisName;
	}
	public void setKpisName(String kpisName) {
		this.kpisName = kpisName;
	}
	public long getValue() {
		return value;
	}
	public void setValue(long value) {
		this.value = value;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
}
