package com.protocolsoft.alarm.bean;
/**
 * @ClassName: AlarmColumnXLineBean
 * @Description: 告警柱图X轴对象
 * @author chensq
 *
 */
public class AlarmColumnXLineBean {
	 /**
     * @Fields kId : kpi id
     */
	private String kId;
    /**
     * @Fields kpisName : kpi 名称
     */
	private String kpisName;
    /**
     * @Fields kpisDisplayName : kpi 显示名
     */
	private String kpisDisplayName;
	
	/**
     * @Fields longTime : long类型的时间
     */
    private long longTime;
   
    /**
     * @Fields columnstime : 柱图开始时间
     */
    private long columnstime;
    
    /**
     * @Fields columnetime : 柱图结束时间
     */
    private long columnetime;

	public String getkId() {
		return kId;
	}

	public void setkId(String kId) {
		this.kId = kId;
	}

	public String getKpisName() {
		return kpisName;
	}

	public void setKpisName(String kpisName) {
		this.kpisName = kpisName;
	}

	public String getKpisDisplayName() {
		return kpisDisplayName;
	}

	public void setKpisDisplayName(String kpisDisplayName) {
		this.kpisDisplayName = kpisDisplayName;
	}

	public long getLongTime() {
		return longTime;
	}

	public void setLongTime(long longTime) {
		this.longTime = longTime;
	}

	public long getColumnstime() {
		return columnstime;
	}

	public void setColumnstime(long columnstime) {
		this.columnstime = columnstime;
	}

	public long getColumnetime() {
		return columnetime;
	}

	public void setColumnetime(long columnetime) {
		this.columnetime = columnetime;
	}
    
}
