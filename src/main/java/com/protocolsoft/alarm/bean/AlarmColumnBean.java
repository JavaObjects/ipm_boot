/**
 *<p>Copyright © 北京协软科技有限公司版权所有。</p>
 *类名:AlarmColumnBean
 *创建人:chensq    创建时间:2017年12月13日
 */
package com.protocolsoft.alarm.bean;

import java.util.List;

/**
 * @ClassName: AlarmColumnBean
 * @Description: 告警柱图对象
 * @author chensq
 *
 */
public class AlarmColumnBean {
    
    /**
     * @Fields alarmColumnXLineBeanList : X轴集合
     */
    private List<AlarmColumnXLineBean> alarmColumnXLineBeanList;
    
    /**
     * @Fields alarmColumnDataBeanList : 数据集合
     */
    private List<AlarmColumnDataBean> alarmColumnDataBeanList;

	public List<AlarmColumnXLineBean> getAlarmColumnXLineBeanList() {
		return alarmColumnXLineBeanList;
	}

	public void setAlarmColumnXLineBeanList(
			List<AlarmColumnXLineBean> alarmColumnXLineBeanList) {
		this.alarmColumnXLineBeanList = alarmColumnXLineBeanList;
	}

	public List<AlarmColumnDataBean> getAlarmColumnDataBeanList() {
		return alarmColumnDataBeanList;
	}

	public void setAlarmColumnDataBeanList(
			List<AlarmColumnDataBean> alarmColumnDataBeanList) {
		this.alarmColumnDataBeanList = alarmColumnDataBeanList;
	}
  
    
}
