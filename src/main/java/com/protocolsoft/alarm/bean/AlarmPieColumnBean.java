/**
 *<p>Copyright © 北京协软科技有限公司版权所有。</p>
 *类名:AlarmPieColumnBean
 *创建人:chensq    创建时间:2019年03月02日
 */
package com.protocolsoft.alarm.bean;

import java.util.List;
/**
 * @ClassName: AlarmPieColumnBean
 * @Description: 告警饼图返回
 * @author chensq
 *
 */
public class AlarmPieColumnBean {
	
	private String unit;
	private Long starttime;
	private Long endtime;
	private String plotName;
	private String type;
	private List<AlarmPieBean> data;
	
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public Long getStarttime() {
		return starttime;
	}
	public void setStarttime(Long starttime) {
		this.starttime = starttime;
	}
	public Long getEndtime() {
		return endtime;
	}
	public void setEndtime(Long endtime) {
		this.endtime = endtime;
	}
	public String getPlotName() {
		return plotName;
	}
	public void setPlotName(String plotName) {
		this.plotName = plotName;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public List<AlarmPieBean> getData() {
		return data;
	}
	public void setData(List<AlarmPieBean> data) {
		this.data = data;
	}
    
}
