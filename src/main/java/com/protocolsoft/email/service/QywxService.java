/**<p>Copyright © 北京协软科技有限公司版权所有。</p>
 * 
 */
package com.protocolsoft.email.service;



import com.protocolsoft.email.bean.QywxBean;

/**
 * 
 * @ClassName: QywxService
 * @author 刘斌
 *
 */
public interface QywxService {
    /**
     * 
     * @Title: updateQywx
     * @param bean
     * @return int
     * @author 刘斌
     */
    int updateQywx(QywxBean bean);
    
    /**
     * 
     * @Title: getQywx
     * @return QywxBean
     * @author 刘斌
     */
    QywxBean getQywx();
}
