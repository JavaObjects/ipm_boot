package com.protocolsoft.email.bean;
/**
 *<p>Copyright © 北京协软科技有限公司版权所有。</p>
 */
/**
 * Copyright 2019 bejson.com 
 */

public class QywxBean {

  private String qywxId;
  private String qywxAppAgentId;
  private String qywxAppSecret;
  private String qywxDepId;
  private String qywxUsers;
  public void setQywxId(String qywxId) {
    this.qywxId = qywxId;
  }
  public String getQywxId() {
    return qywxId;
  }

  public void setQywxAppAgentId(String qywxAppAgentId) {
    this.qywxAppAgentId = qywxAppAgentId;
  }
  public String getQywxAppAgentId() {
    return qywxAppAgentId;
  }

  public void setQywxAppSecret(String qywxAppSecret) {
    this.qywxAppSecret = qywxAppSecret;
  }
  public String getQywxAppSecret() {
    return qywxAppSecret;
  }

  public void setQywxDepId(String qywxDepId) {
    this.qywxDepId = qywxDepId;
  }
  public String getQywxDepId() {
    return qywxDepId;
  }

  public void setQywxUsers(String qywxUsers) {
    this.qywxUsers = qywxUsers;
  }
  public String getQywxUsers() {
    return qywxUsers;
  }

}
