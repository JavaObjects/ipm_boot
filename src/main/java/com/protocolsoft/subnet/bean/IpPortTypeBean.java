/**
 *<p>Copyright © 北京协软科技有限公司版权所有。</p>
 *类名: IpPortTypeBean.java
 *创建人: WWW    创建时间: 2019年5月8日
 */
package com.protocolsoft.subnet.bean;

/**
 * @ClassName: IpPortTypeBean
 * @Description: 
 * @author WWW
 *
 */
public class IpPortTypeBean {

    /**
     * 开始ip
     */
    private long start;
    
    /**
     * 结束IP
     */
    private long end;
    
    /**
     * 1: /
     * 2: -
     * 3: 单
     */
    private int type;

    /**
     * <br />获取 <font color="red"><b>开始ip<b/></font>
     * @return start 开始ip
     */
    public long getStart() {
        return start;
    }
    

    /**  
     * <br />设置 <font color='#333399'><b>开始ip</b></font>
     * @param start 开始ip  
     */
    public void setStart(long start) {
        this.start = start;
    }
    

    /**
     * <br />获取 <font color="red"><b>结束IP<b/></font>
     * @return end 结束IP
     */
    public long getEnd() {
        return end;
    }
    

    /**  
     * <br />设置 <font color='#333399'><b>结束IP</b></font>
     * @param end 结束IP  
     */
    public void setEnd(long end) {
        this.end = end;
    }
    

    /**
     * <br />获取 <font color="red"><b>1:2:-3:单<b/></font>
     * @return type 1:2:-3:单
     */
    public int getType() {
        return type;
    }
    

    /**  
     * <br />设置 <font color='#333399'><b>1:2:-3:单</b></font>
     * @param type 1:2:-3:单  
     */
    public void setType(int type) {
        this.type = type;
    }
    
}
