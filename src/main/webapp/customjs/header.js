/**
 * Created by yanbo on 2017/12/19.
 */
$(function(){
    var
    
    // 数据服务器 后提示
    searchXpmidTitle = "",
    
    // 隧道编号 后提示
    tunnelIdSearchTitle = ""; 
    
    
    /*****************头部内容************************/
    /**
     * 封装头部内容 实现在各个页面可配的功能
     * 1 menu-toggle
     * 2 logo
     * 3 media-body
     * 4 top-menu
     *   logOut
     * 5 timeback
     * 6 help
     * 7 refresh
     * 8 serverTime or shwowTimeEle
     * 9 search
     *
     */
    var header = {
        /**
         * 获取URL参数
         */
        getUrlParams : function() {
            var url = location.search,
                params = new Object();
            if (url.indexOf("?") != -1) {
                var arr = url.substr(1).split("&"), tmp = null;
                for (var i = 0, len = arr.length; i < len; i ++) {
                    tmp = arr[i].split("=");
                    params[tmp[0]] = tmp[1];
                }
            }
            return params;
        },
        /**
         * 每个页面的基本元素
         */
        standard:function(){
            var html = '<a href="" id="menu-toggle"></a>'+
                '<a class="logo pull-left" href="netCockpit.html">'+
                '<img src="images/logo.png" alt="logo">'+
                '</a>';
            return html;
        },
        strMediaBody:function(){
            var html = '<div class="media-body"><div class="media" id="top-menu">';
            return html;
        },
        endMediaBody:function(){
            return '</div></div>';
        },
        
        /**
         * 全局搜索？提示
         * searchXpmidTitle: 数据服务器 后提示
         * tunnelIdSearchTitle: 隧道编号 后提示
         */
        searchPanelSetTitle: function() {
            $("#searchXpmidTitle").attr("title", searchXpmidTitle);
            $("#tunnelIdSearchTitle").attr("title", tunnelIdSearchTitle);
        },
        
        logOut:function(){
            var html = '<div class="pull-right tm-icon" style="border-left:1px solid rgba(255,255,255,0.15);">'+
                '<a data-drawer="logOut" class="drawer-toggle logout-header" href="#logoutMheader">'+
                '<i class="lnr lnr-power-switch" style="font-size: 27px;margin-top: -3px;"></i>'+
                '<span class="logOutSpan">注销</span>'+
                '</a>'+
                '</div>';
            return html;
        },
        logOutModal:function(){
            var html = '<div class="modal fade" id="logoutMheader" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">'+
                '<div class="modal-dialog" style="width: 20%;">'+
                '<div class="modal-content">'+
                '<div class="modal-header logout-header">'+
                '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">'+
               ' &times;'+
            '</button>'+
            '<h4 class="modal-title" id="myModalLabel">'+
                '温馨提示'+
                '</h4>'+
                '</div>'+
                '<div class="modal-body text-center">'+
                '您确定要退出系统吗？'+
            '</div>'+
            '<div class="modal-footer logout-footer" style="text-align: center;">'+
                '<button type="button" class="btn btn-primary logout-btn">'+
                '确定'+
                '</button>'+
                '<button type="button" class="btn btn-default logout-false" data-dismiss="modal" style="right: 0%">'+
               ' 取消'+
                '</button>'+
                '</div>'+
                '</div>'+
                '</div>'+
                '</div>';
            return html;
        },
        timeToback:function(){
            var html = '<div class="pull-left tm-icon">'+
                '<a data-drawer="times" class="drawer-toggle" href="">'+
                '<i class="sa-top-time" style="font-size: 21px;padding-top: 1px;"></i>'+
                '<span>时间回溯</span>'+
                '</a>'+
                '</div>';
            return html;
        },
        helpEle:function(){
            var html = '<div class="pull-left tm-icon">'+
                '<a data-drawer="help" class="drawer-toggle qiteye" href="">'+
                '<i class="lnr lnr-question-circle"></i>'+
                '<span>帮助</span>'+
                '</a>'+
                '</div>';
            return html;
        },
        helpnEle: function() { //网络驾驶舱  
            var html = '<div class="pull-left tm-icon">' +
                '<a data-drawer="help" class="drawer-toggle" href="">' +
                '<i class="lnr lnr-question-circle"></i>' +
                '<span>帮助</span>' +
                '</a>' +
                '</div>';
            return html;
        },
        refreshEle:function(){
            var html = '<div class="pull-left tm-icon">'+
                '<a data-drawer="refresh" id="refreChTab" class="drawer-toggle" href="">'+
                '<i class="lnr lnr-redo"></i>'+
                '<span>刷新</span>'+
                '</a>'+
                '</div>';
            return html;
        },
        serverTimeEle:function(){
            var html = '<div id="time" class="pull-right">'+
                '<span id="hours"></span>:'+
                '<span id="min"></span>:'+
                '<span id="sec"></span>'+
                '</div>';
            return html;
        },
        /**
         * 每个页面的名字
         * @returns {string}
         */
        cockNameZh:function(){
            var titleText = header.getUrlParams().nameZh?decodeURI(header.getUrlParams().nameZh):$("title").text(),
                html = '<div class="pull-right" id="cockNameZh" style="font-size: 16px;margin-top: 13px;letter-spacing: 2px;">' +
                    '<span style="padding-bottom: 3px;border-bottom: 1px solid #fff;">'
                    +titleText+'</span></div>';
            return html;
        },
        showTimeEle:function(){
          var html = '<div class="pull-right timeBackText" style="font-size: 16px;padding-top: 15px; margin-right: 11px;">'+
              '</div>';
            return html;
        },
        /**
         * 此元素放最后
         */
        searchEle:function(){
        	var html = '<div class="pull-left tm-icon">'+
            '<a data-drawer="search" class="drawer-toggle" href="">'+
            '<i class="lnr lnr-magnifier" style="font-size: 21px;padding-top: 1px;"></i>'+
            '<span>搜索查询</span>'+
            '</a>'+
            '</div>';
        	
        	// html += '<div class="pull-left tm-icon">'+
          //   '<a data-drawer="packetPlayback" class="drawer-toggle" href="">'+
          //   '<i class="lnr lnr-chevron-right-circle" style="font-size: 21px;padding-top: 1px;"></i>'+
          //   '<span>数据包回放</span>'+
          //   '</a>'+
          //   '</div>';
        	// 
        	// html += '<div class="pull-left tm-icon">'+
          //   '<a data-drawer="dataImport" class="drawer-toggle" href="">'+
          //   '<i class="lnr lnr-enter" style="font-size: 21px;padding-top: 1px;"></i>'+
          //   '<span>数据导入</span>'+
          //   '</a>'+
          //   '</div>';
        	
            return html;
        },
        topoSearch:function(){
            var html = '<div class="pull-left tm-icon">'+
                '<a data-drawer="topoSearch" class="drawer-toggle" href="">'+
                '<i class="sa-top-topo"></i>'+
                '<span>拓朴查询</span>'+
                '</a>'+
                '</div>';
            return html;
        },
        lockStatus:function(){
            var html = '<div class="pull-left tm-icon">'+
                '<a data-drawer="lockStatus" class="drawer-toggle" href="">'+
                '<i class="sa-top-lock"></i>'+
                '<span>锁定</span>'+
                '</a>'+
                '</div>';
            return html;
        },
        fullScreen:function(){
            var html = '<div class="pull-left tm-icon">'+
                '<a data-drawer="fullScreen" class="drawer-toggle" href="">'+
                '<i class="sa-top-fScreen"></i>'+
                '<span>全屏</span>'+
                '</a>'+
                '</div>';
            return html;
        },
        headerEle:function(){
            var _t = this,
                html = "";
            html += _t.standard();
            html += _t.strMediaBody();
            html += _t.logOut();
            html += _t.timeToback();
            html += _t.refreshEle();
            html += _t.topoSearch();
            return html;
        },
        /**
         * 时间回溯弹出元素
         * @returns {string}
         */
        timesBoxEle:function(){
            var html = '<div id="times" class="tile drawer animated">\n' +
                '            <div class="listview narrow">\n' +
                '                <div class="media">\n' +
                '                    <a>时间回溯</a> <span class="drawer-close">&times;</span>\n' +
                '                </div>\n' +
                '                <div class="overflow text-center">\n' +
                '                    <div class="col-md-12">\n' +
                '                        <div class="datep">\n' +
                '                            <label class="col-md-1 navtime">开始时间</label>\n' +
                '                            <input class="col-md-2 datainp inptime" id="inpstart" type="text" size="50">\n' +
                '                            <label class="col-md-1 navtime">结束时间</label>\n' +
                '                            <input class=" col-md-2 datainp inptime" id="inpend" type="text" size="50">\n' +
                '                            <button class="col-md-1 btn btn-sm timesure">确定</button>\n' +
                '                        </div>\n' +
                '                    </div>\n' +
                '                </div>\n' +
                '            </div>\n' +
                '        </div>';
            return html;
        },
        /**
         * 网络驾驶舱帮助按钮弹出元素
         * @returns {string}
         */
        helpBoxEle: function() {
            var html = '<div id="help" class="tile drawer animated">\n' +
                '            <div class="listview narrow">\n' +
                '                <div class="media">\n' +
                '                    <a>本页使用流程</a> <span class="drawer-close">&times;</span>\n' +
                '                </div>\n' +
                '                <div class="overflow text-left">\n' +
                '                    <div class="col-md-12">\n' +
                '                        <div class="datep">\n' +
                '                            <div class="datep helpcontent">点击左侧监控对象设置 > 进入监控设置页面建立监控设备 > 返回首页 > 点击页面上四个下拉框，选择需要查看的对象 > 页面上的列表及图形双击可以进入告警页面或者通信对页面</div>\n' +
                '                            <button class="col-md-1 btn btn-sm hlepsure">详细文档请点击……</button>\n' +
                '                        </div>\n' +
                '                    </div>\n' +
                '                </div>\n' +
                '            </div>\n' +
                '        </div>';
            return html;
        },
        /**
         * 监控对象页面帮助按钮弹出元素
         * @returns {string}
         */
        helpcBoxEle: function() {
            var html = '<div id="help" class="tile drawer animated" style="height: 250px" >\n' +
                '            <div class="listview narrow">\n' +
                '                <div class="media">\n' +
                '                    <a>本页使用流程</a> <span class="drawer-close">&times;</span>\n' +
                '                </div>\n' +
                '                <div class="overflow text-left">\n' +
                '                    <div class="col-md-12">\n' +
                '                        <div class="datep">\n' +
                '                            <div class="datep helpcontent">\n' +
                '                              <p>1.首次使用XPM的用户，请按照如下内容逐一操作;</p>\n' +
                '                              <p>2.请特别注意：监控对象列表的右上角，红色的“钟形”功能，点击该功能可以展开告警设置列表！</p>\n' +
                '                              <p>3.展开告警列表后，点击该列表右上角“修改”功能，根据弹出窗口即可完成对该监控指标的告警设置；</p>\n' +
                '                              <p>4.请特别注意：在页面最下面的两个设置功能。应用可用性管理与设置，和驾驶舱管理与设置；</p>\n' +
                '                              <p>5.应用可用性设置：可以实现对服务停止的告警功能；</p>\n' +
                '                              <p>6.驾驶舱管理设置：用户可以创建自定义驾驶舱，创建后刷新浏览器页面，即可看到在左侧第一个功能里，出现了该驾驶舱；</p>\n' +
                '                              <p>7.更详细的使用说明请参见系统管理员手册。</p>\n' +
                '                              <button class="col-md-1 btn btn-sm hlepsure">系统管理员手册请点击……</button>\n' +
                '                            </div>\n' +
                '                        </div>\n' +
                '                    </div>\n' +
                '                </div>\n' +
                '            </div>\n' +
                '        </div>';
            return html;
        },
        /**
         * 统计分析页面帮助按钮弹出元素
         * @returns {string}
         */
        helpObservationBoxEle: function() {
            var html = '<div id="help" class="tile drawer animated" style="height: 260px" >\n' +
                '            <div class="listview narrow">\n' +
                '                <div class="media">\n' +
                '                    <a>This page uses steps</a> <span class="drawer-close">&times;</span>\n' +
                '                </div>\n' +
                '                <div class="overflow text-left">\n' +
                '                    <div class="col-md-12">\n' +
                '                        <div class="datep">\n' +
                '                            <div class="datep helpcontent">\n' +
                '                              <p>1. 首次使用XPM的用户，请按照如下内容逐一理解或操作；</p>\n' +
                '                              <p>2. 统计分析驾驶舱，主要用于对同一监控类型下，不同监控对象的横向统计分析，和对比排序等功能；</p>\n' +
                '                              <p>3. 该列表默认显示的是10分钟内，如果需要回溯统计历史时间，可以使用页面上部的快捷工具栏“时间回溯”功能；</p>\n' +
                '                              <p>4. 点击列表右上角的“列”功能，选择需要显示在列表里的指标项；</p>\n' +
                '                              <p>5. 点击列表右上角“导出数据”功能，选择需要导出的文件格式，可以将列表中的内容进行导出；</p>\n' +
                '                              <p>6. 列表中，每个指标的数字，均可以继续钻取；</p>\n' +
                '                              <p>7. 列表中，点击第一列“XXX名称”，列表下面的图形会做相应改变，每个图形也都可以通过双击进行钻取。</p>\n' +
                '                              <button class="col-md-1 btn btn-sm hlepsure">System administrator manual，please click...</button>\n' +
                '                            </div>\n' +
                '                        </div>\n' +
                '                    </div>\n' +
                '                </div>\n' +
                '            </div>\n' +
                '        </div>';
            return html;
        },
        /**
         * 网络驾驶舱帮助按钮弹出元素
         * @returns {string}
         */
        helpnBoxEle: function() {
            var html = '<div id="help" class="tile drawer animated" style="height: 300px" >\n' +
                '            <div class="listview narrow">\n' +
                '                <div class="media">\n' +
                '                    <a>本页使用流程</a> <span class="drawer-close">&times;</span>\n' +
                '                </div>\n' +
                '                <div class="overflow text-left">\n' +
                '                    <div class="col-md-12">\n' +
                '                        <div class="datep">\n' +
                '                            <div class="datep helpcontent">\n' +
                '                              <p>1.首次使用XPM的用户，请按照如下内容逐一操作;</p>\n' +
                '                              <p>2.通过改变页面最上部的下拉菜单（从左至右）内容，可以改变本页面的显示内容；</p>\n' +
                '                              <p>3.本页面内的每个图形，或数字，都可以通过双击钻取到下一级页面；</p>\n' +
                '                              <p>4.将页面下拉到最下面，可以看到一个“+”，双击该“+”，可以增减本页面的监控内容；</p>\n' +
                '                              <p>5.左上角LOGO右侧，是重要的常用功能；特别是第1、3、6功能，请特别注意；</p>\n' +
                '                              <p>6.浏览器左下角的绿色圆形功能是在线人工帮助！有任何XPM问题都可以咨询在线专家；</p>\n' +
                '                              <p>7.浏览器右下角的半透明“告警信息”，显示的是最近发生的，未响应的告警信息；</p>\n' +
                '                              <p>8.浏览器右侧滚动条上，齿轮状功能为皮肤设置，可以改变XPM的页面背景。</p>\n' +
                '                              <p>9.更详细的使用说明请参见系统管理员手册。</p>\n' +
                '                              <button class="col-md-1 btn btn-sm hlepsure">系统管理员手册请点击……</button>\n' +
                '                            </div>\n' +
                '                        </div>\n' +
                '                    </div>\n' +
                '                </div>\n' +
                '            </div>\n' +
                '        </div>';
            return html;
        },
        /**
         * 会话列表页面帮助按钮弹出元素
         * @returns {string}
         */
        helpSessionBoxEle: function() {
            var html = '<div id="help" class="tile drawer animated" style="height: 260px" >\n' +
                '            <div class="listview narrow">\n' +
                '                <div class="media">\n' +
                '                    <a>本页使用流程</a> <span class="drawer-close">&times;</span>\n' +
                '                </div>\n' +
                '                <div class="overflow text-left">\n' +
                '                    <div class="col-md-12">\n' +
                '                        <div class="datep">\n' +
                '                            <div class="datep helpcontent">\n' +
                '                              <p>1.首次使用XPM的用户，请按照如下内容逐一操作;</p>\n' +
                '                              <p>2.拖拽图形中任意一个局部（波峰或波谷），页面下方会出现与之对应的通信对或会话列表；</p>\n' +
                '                              <p>3.在拖拽后出现的列表的右上角，点击第一个“九宫格”形状的功能，可以增减列表的列内容；</p>\n' +
                '                              <p>4.如果是通信对列表，则可继续点击任意一条聚合通信对，钻取10秒粒度的通信对信息列表；</p>\n' +
                '                              <p>5.在10秒粒度通信对列表，右上角最后一个功能，是重要的原始数据包下载功能，如果已经开启该功能，可以在这里下载通信对的原始数据包；</p>\n' +
                '                              <p>6.页面左下部的“聚合分析”功能，是非常实用的多维度（第一个下拉菜单）统计分析功能；</p>\n' +
                '                              <p>7.更详细的使用说明请参见系统管理员手册。</p>\n' +
                '                              <button class="col-md-1 btn btn-sm hlepsure">系统管理员手册请点击……</button>\n' +
                '                            </div>\n' +
                '                        </div>\n' +
                '                    </div>\n' +
                '                </div>\n' +
                '            </div>\n' +
                '        </div>';
            return html;
        },
        /**
         * 系统设置面帮助按钮弹出元素
         * @returns {string}
         */
        helpSettingBoxEle: function() {
            var html = '<div id="help" class="tile drawer animated" style="height: 230px" >\n' +
                '            <div class="listview narrow">\n' +
                '                <div class="media">\n' +
                '                    <a>本页使用流程</a> <span class="drawer-close">&times;</span>\n' +
                '                </div>\n' +
                '                <div class="overflow text-left">\n' +
                '                    <div class="col-md-12">\n' +
                '                        <div class="datep">\n' +
                '                            <div class="datep helpcontent">\n' +
                '                              <p>1.首次使用XPM的用户，请按照如下内容逐一操作；</p>\n' +
                '                              <p>2.接入网络流量后，如果不知道哪个是工作网卡，可以在“网卡状态”里，通过查看数据包数量获得；</p>\n' +
                '                              <p>3.数据包去重：该功能默认是关闭的；但在完成镜像流量接入，并验证完流量大小和网管一致后，需要开启该功能，以确保没有重复流量干扰计算；</p>\n' +
                '                              <p>4.云管理设置：如果需要我们提供远端监控服务，请与左下角的在线服务专家联系，获取云端服务地址；</p>\n' +
                '                              <p>5.内网网段设置：内网网段，是指XPM部署位置后面的服务器区地址，请务必准确且完整的填写，这一设置将与多个指标计算有关！非常重要！</p>\n' +
                '                              <p>6.更详细的使用说明请参见系统管理员手册。</p>\n' +
                '                              <button class="col-md-1 btn btn-sm hlepsure">系统管理员手册请点击……</button>\n' +
                '                            </div>\n' +
                '                        </div>\n' +
                '                    </div>\n' +
                '                </div>\n' +
                '            </div>\n' +
                '        </div>';
            return html;
        },
        /**
         * 拓扑页面帮助按钮弹出元素
         * @returns {string}
         */
        helpJnodeBoxEle: function() {
            var html = '<div id="help" class="tile drawer animated" style="height: 260px" >\n' +
                '            <div class="listview narrow">\n' +
                '                <div class="media">\n' +
                '                    <a>本页使用流程</a> <span class="drawer-close">&times;</span>\n' +
                '                </div>\n' +
                '                <div class="overflow text-left">\n' +
                '                    <div class="col-md-12">\n' +
                '                        <div class="datep">\n' +
                '                            <div class="datep helpcontent">\n' +
                '                              <p>1.首次使用XPM的用户，请按照如下内容逐一操作；</p>\n' +
                '                              <p>2.请将鼠标放置在右上角的蓝色工具条上，了解每个功能的内容；再使用鼠标滚轮，或把鼠标挪到圆点上面，查看变化；</p>\n' +
                '                              <p>3.拓扑里的绿色圆点代表IP，蓝色圆点代表PORT；</p>\n' +
                '                              <p>4.双击IP圆点，可以发现该IP正在通信的PORT；继续双击PORT圆点，可以发现与之通信的其他IP；</p>\n' +
                '                              <p>5.如果需要查找某个IP的通信关系，可以在左侧“服务端IP”或“客户端IP”输入需要查找的IP，确定后，页面会只显示该IP:PORT，然后重复第3项的操作；</p>\n' +
                '                              <p>6.更详细的使用说明请参见系统管理员手册。</p>\n' +
                '                              <button class="col-md-1 btn btn-sm hlepsure">系统管理员手册请点击……</button>\n' +
                '                            </div>\n' +
                '                        </div>\n' +
                '                    </div>\n' +
                '                </div>\n' +
                '            </div>\n' +
                '        </div>';
            return html;
        },
        /**
         * 自定义页面帮助按钮弹出元素
         * @returns {string}
         */
        helpCockpitBoxEle: function() {
            var html = '<div id="help" class="tile drawer animated" style="height: 260px" >\n' +
                '            <div class="listview narrow">\n' +
                '                <div class="media">\n' +
                '                    <a>本页使用流程</a> <span class="drawer-close">&times;</span>\n' +
                '                </div>\n' +
                '                <div class="overflow text-left">\n' +
                '                    <div class="col-md-12">\n' +
                '                        <div class="datep">\n' +
                '                            <div class="datep helpcontent">\n' +
                '                              <p>1.首次使用XPM的用户，请按照如下内容逐一操作;</p>\n' +
                '                              <p>2.首次打开刚创建的自定义驾驶舱，页面是空白的，需要用户自行建立监控内容和版面；</p>\n' +
                '                              <p>3.从左侧第5个图标开始，是可编辑的监控对象；点击“观察点”，空白页面上会出现观察点图标，再双击该图标，弹出绘图列表页面；</p>\n' +
                '                              <p>4.添加多个小列表后，将鼠标放置在小列表边框线的中点位置，会出现连接提示，可以将不同监控对象的小列表，按照拓扑或逻辑相连接；</p>\n' +
                '                              <p>5.自定义驾驶舱设计完成后，请务必点击工具栏的保存功能；</p>\n' +
                '                              <p>6.自定义驾驶舱里的小列表，和图形，都可以钻取到相应页面；</p>\n' +
                '                              <p>7.更详细的使用说明请参见系统管理员手册。</p>\n' +
                '                              <button class="col-md-1 btn btn-sm hlepsure">系统管理员手册请点击……</button>\n' +
                '                            </div>\n' +
                '                        </div>\n' +
                '                    </div>\n' +
                '                </div>\n' +
                '            </div>\n' +
                '        </div>';
            return html;
        },
        /**
         * 报表计划页面帮助按钮弹出元素
         * @returns {string}
         */
        helpfBoxEle: function() {
            var html = '<div id="help" class="tile drawer animated">\n' +
                '            <div class="listview narrow">\n' +
                '                <div class="media">\n' +
                '                    <a>本页使用流程</a> <span class="drawer-close">&times;</span>\n' +
                '                </div>\n' +
                '                <div class="overflow text-left">\n' +
                '                    <div class="col-md-12">\n' +
                '                        <div class="datep">\n' +
                '                            <div class="datep helpcontent">点击左侧报表模板  > 添加报表模板 > 返回报表计划页面 > 添加、修改、删除报表计划</div>\n' +
                '                            <button class="col-md-1 btn btn-sm hlepsure">详细文档请点击……</button>\n' +
                '                        </div>\n' +
                '                    </div>\n' +
                '                </div>\n' +
                '            </div>\n' +
                '        </div>';
            return html;
        },
        /**
         * 刷新粒度弹出元素
         * @returns {string}
         */
        refreshBoxEle:function(){
            var html ='\t\t<div id="refresh" class="tile drawer animated">\n' +
                '\t\t\t<div class="listview narrow">\n' +
                '\t\t\t\t<div class="media">\n' +
                '\t\t\t\t\t<a>刷新粒度</a> <span class="drawer-close">&times;</span>\n' +
                '\t\t\t\t</div>\n' +
                '\t\t\t\t<div class="overflow text-center">\n' +
                '\t\t\t\t\t<div class="col-md-12 text-left">\n' +
                // '\t\t\t\t\t\t<input class="cursor" type="radio" name="timelidu" value="0">立即刷新&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\n' +
                '\t\t\t\t\t\t<input class="cursor none" type="radio" name="timelidu" value="0"><a href="" class="a_refreshData">立即刷新</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\n' +
                '\t\t\t\t\t\t<input class="cursor" type="radio" name="timelidu" value="1">1秒&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\n' +
                '\t\t\t\t\t\t<input class="cursor" type="radio" name="timelidu" value="10">10秒&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\n' +
                '\t\t\t\t\t\t<input class="cursor" type="radio" name="timelidu" value="30">30秒&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\n' +
                '\t\t\t\t\t\t<input class="cursor" type="radio" name="timelidu" value="60">60秒&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\n' +
                '\t\t\t\t\t</div>\n' +
                '\t\t\t\t</div>\n' +
                '\t\t\t</div>\n' +
                '\t\t</div>';
            return html;
        },
        /**
         * 搜索查询弹出元素
         * @returns {string}
         */
        searchBoxEle:function(){
            var html = '     <div id="search" class="tile drawer animated" style="height: 410px;">\n' +
                '            <div class="listview narrow">\n' +
                '                <div class="media">\n' +
                '                    <a>搜索查询</a>\n' +
                '                    <span class="drawer-close">&times;</span>\n' +
                '                </div>\n' +
                '                <div class="overflow text-right">\n' +
                '                    <div class="col-md-12" style="margin-top: 10px; text-align: left; line-height: 34px;">\n' +
                '                        <label style="text-align: right;" class="col-md-1 navtime" for="xpmid">数据服务器</label>\n' +
                '                            <select id="xpmid" class="col-md-2 inptime searchInput">\n' +
                '                                <option value="0">请选择...</option>\n' +
                '                            </select>\n' +
                '                            <i id="searchXpmidTitle" style="font-size: 16px; margin-left: 10px;" class="lnr lnr-question-circle"></i>' +
                '                    </div>\n' +
                '                    <div class="col-md-12" style="margin-top: 10px;">\n' +
                '                        <div class="datep">\n' +
                '                            <label class="col-md-1 navtime">开始时间</label>\n' +
                '                            <input class="col-md-2 datainp inptime searchInput" id="searchstart" type="text">\n' +
                '                            <label class="col-md-1 navtime">结束时间</label>\n' +
                '                            <input class=" col-md-2 datainp inptime searchInput" id="searchend" type="text">\n' +
                '                        </div>\n' +
                '                    </div>\n' +
                '                    <div class="col-md-12" style="margin-top: 10px;">\n' +
                '                            <label class="col-md-1 navtime" for="tunnelProtocolSearch">隧道协议</label>\n' +
                '                            <select id="tunnelProtocolSearch" class="col-md-2 inptime searchInput">\n' +
                '                                <option value="0">请选择...</option>\n' +
                '                                <option value="vxlan">VXLAN</option>\n' +
                '                                <option value="gre">GRE</option>\n' +
                '                                <option value="mpls">MPLS</option>\n' +
                '                                <option value="vlan">VLAN</option>\n' +
                '                            </select>\n' +
                '                    </div>\n' +
                '                    <div class="col-md-12" style="margin-top: 10px; text-align: left; line-height: 34px;"">\n' +
                '                            <label style="text-align: right;" class="col-md-1 navtime" for="tunnelSourceipSearch">隧道源IP</label>\n' +
                '                            <input class="col-md-2 inptime searchInput" type="text" id="tunnelSourceipSearch">\n' +
                '\n' +
                '                            <label style="text-align: right;" class="col-md-1 navtime" for="tunnelDestipSearch">隧道目的IP</label>\n' +
                '                            <input class="col-md-2 inptime searchInput" type="text" id="tunnelDestipSearch">\n' +
                '\n' +
                '                            <label style="text-align: right;" class="col-md-1 navtime" for="tunnelIdSearch">隧道编号</label>\n' +
                '                            <input class="col-md-2 inptime searchInput" type="text" id="tunnelIdSearch">\n' +
                '                            <i id="tunnelIdSearchTitle" style="font-size: 16px; margin-left: 10px;" class="lnr lnr-question-circle"></i>' +
                '                    </div>\n' +
                '                    <div class="col-md-12" style="margin-top: 10px;">\n' +
                '                            <label class="col-md-1 navtime" for="watchpointSearch">观察点/采集点</label>\n' +
                '                            <select id="watchpointSearch" class="col-md-2 inptime searchInput">\n' +
                '                                <option value="0">请选择...</option>\n' +
                '                            </select>\n' +
                '                            \n' +
                '                            <label class="col-md-1 navtime" for="serveripSearch">服务端IP</label>\n' +
                '                            <input class="col-md-2 inptime searchInput" type="text" id="serveripSearch">\n' +
                '\n' +
                '                            <label class="col-md-1 navtime" for="clientipSearch">客户端IP</label>\n' +
                '                            <input class="col-md-2 inptime searchInput" type="text" id="clientipSearch">\n' +
                '                    </div>\n' +
                '                    <div class="col-md-12" style="margin-top: 12px;">\n' +
                '                            <label class="col-md-1 navtime" for="urlSearch">URL</label>\n' +
                '                            <input class="col-md-2 inptime searchInput" type="text" id="urlSearch">\n' +
                '\n' +
                '                            <label class="col-md-1 navtime" for="sqlSearch">SQL</label>\n' +
                '                            <input class="col-md-2 inptime searchInput" type="text" id="sqlSearch">\n' +
                '                            <div class="col-md-3 text-left" style="margin-top: 6px; margin-left: 33px;">\n' +
                '\t\t\t\t\t\t\t\t<input class="searchInput" type="radio" name="radio" value="1" checked>ORACLE&nbsp;&nbsp;&nbsp;&nbsp;\n' +
                '\t\t\t\t\t\t\t\t<input class="searchInput" type="radio" name="radio" value="2">MYSQL&nbsp;&nbsp;&nbsp;&nbsp;\n' +
                '\t\t\t\t\t\t\t\t<input class="searchInput" type="radio" name="radio" value="3">SQLSERVER\n' +
                '                            </div>\n' +
                '                    </div>\n' +
                '                    <div class="col-md-12" style="margin-top: 12px;">\n' +
                '                            <label class="col-md-1 navtime" for="messagessSearch">报文</label>\n' +
                '                            <input class="col-md-2 inptime searchInput" type="text" id="messagessSearch">\n' +
                '                    </div>\n' +
                '                    <div class="col-md-1" style="margin-top: 12px;">\n' +
                '                        <button class="btn btn-sm" id="searchTimesure">确定</button>\n' +
                '                        <button class="btn btn-sm" id="searchTimeClose">取消</button>\n' +
                '                </div>\n' +
                '                </div>\n' +
                '            </div>\n' +
                '        </div>';
            return html;
        },
        /**
         * 数据导入弹出元素
         * @returns {string}
         */
        dataImportBoxEle:function(){
            var html = '     <div id="dataImport" class="tile drawer animated" style="height: 150px;">\n' +
                '            <div class="listview narrow">\n' +
                '                <div class="media">\n' +
                '                    <a>数据导入</a>\n' +
                '                    <span class="drawer-close">&times;</span>\n' +
                '                </div>\n' +
                '                <div class="overflow">\n' +
                '                    <div class="col-md-12" style="margin-top: 10px;">\n' +
                '                        <label class="col-md-1 navtime"></label>\n' +
                '						<div class="fileupload fileupload-new" data-provides="fileupload">' +
                '							<span class="btn btn-file btn-sm btn-peizhi">' +
                '							<span class="fileupload-new">数据包文件上传</span>' +
                '							<span class="fileupload-exists">选择其它文件</span>' +
                '								<input type="file" id="dataPackageUploadFile">' +
                '						    </span>' +
                '							<span class="fileupload-preview"></span>' +
                '								<a href="#" class="close close-pic fileupload-exists" data-dismiss="fileupload">' +
                '									<i class="fa fa-times"></i>' +
                '								</a>' +
                '								<button class="btn btn-sm m-t-10 m-t-u fileupload-exists" id="dataPackageconfigImport" data-loading-text="正在导入...">上传</button>' +
                '						</div>' +
                '                    </div>\n' +
                '                    <div class="col-md-12" style="margin-top: 10px;">\n' +
                '                        <label class="col-md-1 navtime"></label>\n' +
                '						<div class="fileupload fileupload-new" data-provides="fileupload">' +
                '							<span class="btn btn-file btn-sm btn-peizhi">' +
                '							<span class="fileupload-new">秘钥文件上传</span>' +
                '							<span class="fileupload-exists">选择其它文件</span>' +
                '								<input type="file" id="keyUploadFile">' +
                '						    </span>' +
                '							<span class="fileupload-preview"></span>' +
                '								<a href="#" class="close close-pic fileupload-exists" data-dismiss="fileupload">' +
                '									<i class="fa fa-times"></i>' +
                '								</a>' +
                '								<button class="btn btn-sm m-t-10 m-t-u fileupload-exists" id="keyConfigImport" data-loading-text="正在导入...">上传</button>' +
                '						</div>' +
                '                    </div>\n' +
                '                </div>\n' +
                '            </div>\n' +
                '        </div>';
            return html;
        },
        page:location.pathname.split(".")[0].replace(/\//,""),
        html:"",
        /**
         * 简单跳转页面的html
         */
        nextPage:function () {
            var html = '\t\t<div class="arrNext" id="nextPage">\n' +
                '\t\t\t<a></a>\n' +
                '\t\t</div>';
            $("#content").append(html);
        },
        /**
         * 授权许可
         * @returns {string}
         * @constructor
         */
        license:function(){
            var html = '<div class="pull-right cursor" id="licenseBox" style="font-size: 16px;margin-top: 13px;letter-spacing: 2px;color:red;">' +
                    '</div>';
            return html;
        }
    };
    /**
     *  是否可以跳转到通信队  trafficpair: true | false 若为false将跳转功能隐藏并去掉头部搜索功能
     *  是否可以选择多个观察点 manywatchpoint true | false
     *  是否展示地图 map true | false
     *  是否展拓朴图 topo true | false
     */
    $.ajax({
        url:"/authorizeModuleController/getSelectIsOpen.do",
        method:"POST",
        data:{},
        dataType:"json",
        beforeSend:function (XMLHttpRequest) {},
        success:function (data,textStatus,XMLHttpRequest) {
            $("#header").attr({
                "data-trafficpair":Number(data.trafficpair),   // 是否可以跳转到通信队
                "data-manywatchpoint":Number(data.manywatchpoint),// 是否可以选择多个观察点
                "data-map":Number(data.map),// sidebar是否展示地图
                "data-topo":Number(data.topo) //头部是否展示拓朴图
            })
        },
        error:function (XMLHttpRequest,textStatus,errorThorwn) {
            console.error(XMLHttpRequest);
            console.error(textStatus);
            console.error(errorThorwn);
        },
        complete:function (XMLHttpRequest,textStatus) {
            if($("#header").attr("data-trafficpair") != undefined && !+$("#header").attr("data-trafficpair")){
                $('a[data-drawer="search"]').parent().remove();
                $("#search").remove();
            }
            if($("#header").attr("data-topo") != undefined && !+$("#header").attr("data-topo")){
                $('a[data-drawer="topoSearch"]').parent().remove();
            }
            if($("#header").attr("data-map") != undefined && !+$("#header").attr("data-map")){
                $('a.sa-side-map').parent().remove();
            }
        }
    });
    switch (header.page){
        // case "heatMap":
        case "baowenJy":
            header.html += header.headerEle();
            header.html += header.lockStatus();
            header.html += header.fullScreen();
            header.html += header.showTimeEle();
            header.html += header.cockNameZh();

            header.html += header.license();

            header.html += header.searchEle();
            header.html += header.helpEle();
            header.html += header.endMediaBody();
            $("#content").append($(header.timesBoxEle()));
            $("#content").append($(header.refreshBoxEle())); 
            $("#content").append($(header.searchBoxEle() + header.dataImportBoxEle()));
            header.nextPage();
            break;
        case "userSidekpi":
        case "serverSidekpi":
        case "httpSerkpi":
        case "oracleSerkpi":
        case "mysqlSerkpi":
        case "sqlSerkpi":
        case "urlkpi":
        case "observationPointkpi":
            header.html += header.headerEle();
            header.html += header.lockStatus();
            header.html += header.fullScreen();
            header.html += header.showTimeEle();
            header.html += header.cockNameZh();

            header.html += header.license();

            header.html += header.searchEle();
            header.html += header.helpnEle();
            header.html += header.endMediaBody();
            $("#content").append($(header.timesBoxEle()));
            $("#content").append($(header.refreshBoxEle())); 
            $("#content").append($(header.searchBoxEle() + header.dataImportBoxEle()));
            $("#content").append($(header.helpObservationBoxEle()));
            header.nextPage();
            break;
        case "netCockpit":
            header.html += header.standard();
            header.html += header.strMediaBody();
            header.html += header.logOut();
            header.html += header.timeToback();
            header.html += header.refreshEle();
            header.html += header.topoSearch();
            header.html += header.lockStatus();
            header.html += header.fullScreen();
            header.html += header.showTimeEle();
            header.html += header.cockNameZh();
            
            header.html += header.license();

            header.html += header.searchEle();
            header.html += header.helpnEle();
            header.html += header.endMediaBody();
            $("#content").append($(header.timesBoxEle()));
            $("#content").append($(header.refreshBoxEle())); 
            $("#content").append($(header.searchBoxEle() + header.dataImportBoxEle()));
            $("#content").append($(header.helpnBoxEle()));
            break;
        case "cockpitmanage":
            header.html += header.standard();
            header.html += header.strMediaBody();
            header.html += header.logOut();
            header.html += header.refreshEle();
            header.html += header.topoSearch();
            header.html += header.fullScreen();
            header.html += header.serverTimeEle();
            header.html += header.cockNameZh();

            header.html += header.license();

            header.html += header.searchEle();
            header.html += header.helpnEle();
            header.html += header.endMediaBody();
            $("#content").append($(header.searchBoxEle() + header.dataImportBoxEle()));
            $("#content").append($(header.helpcBoxEle()));
            break;
        case "settingindex":
            header.html += header.standard();
            header.html += header.strMediaBody();
            header.html += header.logOut();
            header.html += header.refreshEle();
            header.html += header.topoSearch();
            header.html += header.fullScreen();
            header.html += header.serverTimeEle();
            header.html += header.cockNameZh();

            header.html += header.license();

            header.html += header.searchEle();
            header.html += header.helpnEle();
            header.html += header.endMediaBody();
            $("#content").append($(header.searchBoxEle() + header.dataImportBoxEle()));
            $("#content").append($(header.helpSettingBoxEle()));
            break;
        case "jnode":
            header.html += header.standard();
            header.html += header.strMediaBody();
            header.html += header.logOut();
            header.html += header.refreshEle();
            header.html += header.topoSearch();
            header.html += header.fullScreen();
            header.html += header.serverTimeEle();
            header.html += header.cockNameZh();

            header.html += header.license();

            header.html += header.searchEle();
            header.html += header.helpnEle();
            header.html += header.endMediaBody();
            $("#content").append($(header.searchBoxEle() + header.dataImportBoxEle()));
            $("#content").append($(header.helpJnodeBoxEle()));
            break;
        case "fromPlan":
            header.html += header.standard();
            header.html += header.strMediaBody();
            header.html += header.logOut();
            header.html += header.refreshEle();
            header.html += header.topoSearch();
            header.html += header.fullScreen();
            header.html += header.serverTimeEle();
            header.html += header.cockNameZh();

            header.html += header.license();

            header.html += header.searchEle();
            header.html += header.helpnEle();
            header.html += header.endMediaBody();
            $("#content").append($(header.searchBoxEle() + header.dataImportBoxEle()));
            $("#content").append($(header.helpfBoxEle()));
            break;
        case "systemCapital":
            header.html += header.headerEle();
            header.html += header.fullScreen();
            header.html += header.showTimeEle();
            header.html += header.cockNameZh();

            header.html += header.license();

            header.html += header.searchEle();
            header.html += header.helpEle();
            header.html += header.endMediaBody();
            $("#content").append($(header.timesBoxEle()));
            $("#content").append($(header.refreshBoxEle()));
            $("#content").append($(header.searchBoxEle() + header.dataImportBoxEle()));
            break;
        case "commun_queue":
        case "alarmSetting":
        case "bssSession":
            //无锁定功能
            header.html += header.headerEle();
            header.html += header.fullScreen();
            header.html += header.showTimeEle();
            header.html += header.cockNameZh();

            header.html += header.license();

            header.html += header.searchEle();
            header.html += header.helpnEle();
            header.html += header.endMediaBody();
            $("#content").append($(header.timesBoxEle()));
            $("#content").append($(header.searchBoxEle() + header.dataImportBoxEle()));
            $("#content").append($(header.helpSessionBoxEle()));
            break;
        case "fromHistory":
            //无锁定功能
            header.html += header.headerEle();
            header.html += header.fullScreen();
            header.html += header.serverTimeEle();
            header.html += header.showTimeEle();
            header.html += header.cockNameZh();

            header.html += header.license();

            header.html += header.searchEle();
            header.html += header.helpEle();
            header.html += header.endMediaBody();
            $("#content").append($(header.timesBoxEle()));
            $("#content").append($(header.searchBoxEle() + header.dataImportBoxEle()));
            break;   
        case "cockpit":
            //时间配置的html不一样
            header.html += header.headerEle();
            header.html += header.lockStatus();
            header.html += header.fullScreen();
            header.html += header.serverTimeEle();
            header.html += header.cockNameZh();

            header.html += header.license();

            header.html += header.searchEle();
            header.html += header.helpnEle();
            header.html += header.endMediaBody();
            $("#content").append($(header.timesBoxEle()));
            $("#content").append($(header.refreshBoxEle()));
            $("#content").append($(header.searchBoxEle() + header.dataImportBoxEle()));
            $("#content").append($(header.helpCockpitBoxEle()));
            break;
        default:
            header.html += header.standard();
            header.html += header.strMediaBody();
            header.html += header.logOut();
            header.html += header.refreshEle();
            header.html += header.topoSearch();
            header.html += header.fullScreen();
            header.html += header.serverTimeEle();
            header.html += header.cockNameZh();

            header.html += header.license();

            header.html += header.searchEle();
            header.html += header.helpEle();
            header.html += header.endMediaBody();
            $("#content").append($(header.searchBoxEle() + header.dataImportBoxEle()));
    }
    $("#header").append($(header.html));
    $("body").eq(0).append(header.logOutModal());
    header.searchPanelSetTitle();
    switch (header.page){
        case "observationPointkpi":
        case "userSidekpi":
        case "serverSidekpi":
        case "httpSerkpi":
        case "oracleSerkpi":
        case "mysqlSerkpi":
        case "sqlSerkpi":
        case "heatMap":
        case "cockpit":
        case "urlkpi":
        case "netCockpit":
            //为头部的锁图标赋值
            $.post("/monitor/getViewById.do",{"id":header.page == "netCockpit"?1:header.getUrlParams().busiId},function(data){
                if(data.lockStatus){
                    //锁定
                    $('a[data-drawer="lockStatus"]').children("i").attr("class","sa-top-lock");
                    $('a[data-drawer="lockStatus"]').children("span").text("锁定");
                    if(header.page!="cockpit"){
                        $("#list-draw").parent().hide();
                    }
                }else {
                    //未锁定
                    $('a[data-drawer="lockStatus"]').children("i").attr("class","sa-top-unlock");
                    $('a[data-drawer="lockStatus"]').children("span").text("解锁");
                }
            },"json");
            break;
    }
    $(".logout-header").click(function(){
        $("#logoutMheader").modal("show");
    });
    /**
     * 控制模块的权限
     * 此处应该将所有模块都默认写上
     * 由此接口控制是否隐藏
     */
    $.ajax({
        url:"/userAuthorize/getJurisModuleList.do",
        method:"POST",
        data:{
            requestType:"get"
        },
        dataType:"json",
        beforeSend:function (XMLHttpRequest) {},
        success:function(data,textStatus,XMLHttpRequest){
            data.forEach(function(item,index){
                if(item.namezh == "拓扑图"){
                    if(!item.checked){
                        $("a[data-drawer='topoSearch']").parent().addClass("none");
                    }
                }
            })
        },
        error:function (XMLHttpRequest,textStatus,errorThorwn) {
            console.error(XMLHttpRequest);
            console.error(textStatus);
            console.error(errorThorwn);
        },
        complete:function (XMLHttpRequest,textStatus) {}
    });

    // 数据包上传
    $("#dataPackageconfigImport").click(function() {
        if (typeof FormData == 'undefined') {
            jeBox.alert("此浏览器不支持上传，请使用高版本或者其他浏览器");
            return;
        }
        var _this = $(this).button("loading");
        var file = $("#dataPackageUploadFile").prop("files")[0];
        var formData = new FormData();
        formData.append("name", "pcapname");
        formData.append("file", file);
        $.ajax({
            url: '/cgi-bin/processPcap.cgi',
            type: 'POST',
            data: formData,
            processData: false,
            contentType: false,
            success: function() {
                jeBox.alert("导入成功");
                _this.button("reset");
            },
            error: function() {
                jeBox.alert("导入失败");
                _this.button("reset");
            }
        });
    });

    // 秘钥文件上传
    $("#keyConfigImport").click(function() {
        if (typeof FormData == 'undefined') {
            jeBox.alert("此浏览器不支持上传，请使用高版本或者其他浏览器");
            return;
        }
        var _this = $(this).button("loading");
        var file = $("#keyUploadFile").prop("files")[0];
        var formData = new FormData();
        formData.append("name", "rsaname");
        formData.append("file", file);
        $.ajax({
            url: '/cgi-bin/processRsa.cgi',
            type: 'POST',
            data: formData,
            processData: false,
            contentType: false,
            success: function() {
                jeBox.alert("导入成功");
                _this.button("reset");
            },
            error: function() {
                jeBox.alert("导入失败");
                _this.button("reset");
            }
        });
    });
    $("a[data-drawer='packetPlayback']").click(function () {
    	window.open(document.location.protocol + "//" + document.domain + ":8888")
    });
    $("a[data-drawer='topoSearch']").click(function () {
        location.href = 'jnode.html?true';
    });
    $("a[data-drawer='lockStatus']").click(function(){
        var _t = $("a[data-drawer='lockStatus']"),
            _status;
        $.post("/user/getUserRole.do",null,function(data){
            if(data==1){
                // 1 为管理员
                if(_t.children("span").text()!="锁定"){
                    _t.children("i").attr("class","sa-top-lock");
                    _t.children("span").text("锁定");
                    _status = 1;
                }else {
                    _t.children("i").attr("class","sa-top-unlock");
                    _t.children("span").text("解锁");
                    _status = 0;
                }
                $.ajax({
                    url:"/monitor/updateViewStatus.do",
                    async:false,
                    method:"POST",
                    data:{
                        id:header.page == "netCockpit" ? 1 : header.getUrlParams().busiId,
                        status:_status
                    },
                    dataType:"json",
                    beforeSend:function (XMLHttpRequest) {},
                    success:function(data,textStatus,XMLHttpRequest){
                        if(data.status){
                            jeBox.alert("修改状态成功");
                            if(header.page!="cockpit" || header.page!="heatMap"){
                                if(_t.children("span").text()!="锁定"){
                                    $("#list-draw").parent().show();
                                }else {
                                    $("#list-draw").parent().hide();
                                }
                            }
                        }
                    },
                    error:function (XMLHttpRequest,textStatus,errorThorwn) {
                        console.error(XMLHttpRequest);
                        console.error(textStatus);
                        console.error(errorThorwn);
                    },
                    complete:function (XMLHttpRequest,textStatus) {}
                });
            }else {
                //||data==2 普通用户
                //此处应该加一层逻辑 判断当前模块是否是非admin 若是则不进下面这个ajax
                var titleText = header.getUrlParams().nameZh?decodeURI(header.getUrlParams().nameZh):$("title").text(),
                    isAdmin = true;
                $.ajax({
                    url:"/userAuthorize/getJurisModuleList.do",
                    method:"POST",
                    async:false,
                    data:{
                        requestType:"get"
                    },
                    dataType:"json",
                    beforeSend:function (XMLHttpRequest) {},
                    success:function(datait,textStatus,XMLHttpRequest){
                        datait.forEach(function(item,index){
                            if(item.namezh == titleText){
                                if(item.userName != "admin" && item.userName != null){
                                    isAdmin = false;
                                }
                            }
                        })
                    },
                    error:function (XMLHttpRequest,textStatus,errorThorwn) {
                        console.error(XMLHttpRequest);
                        console.error(textStatus);
                        console.error(errorThorwn);
                    },
                    complete:function (XMLHttpRequest,textStatus) {}
                });
                if(!isAdmin){
                    if(_t.children("span").text()!="锁定"){
                        _t.children("i").attr("class","sa-top-lock");
                        _t.children("span").text("锁定");
                        _status = 1;
                    }else {
                        _t.children("i").attr("class","sa-top-unlock");
                        _t.children("span").text("解锁");
                        _status = 0;
                    }
                    $.ajax({
                        url:"/monitor/updateViewStatus.do",
                        async:false,
                        method:"POST",
                        data:{
                            id:header.page == "netCockpit" ? 1 : header.getUrlParams().busiId,
                            status:_status
                        },
                        dataType:"json",
                        beforeSend:function (XMLHttpRequest) {},
                        success:function(data,textStatus,XMLHttpRequest){
                            if(data.status){
                                jeBox.alert("修改状态成功");
                                if(header.page!="cockpit" || header.page!="heatMap"){
                                    if(_t.children("span").text()!="锁定"){
                                        $("#list-draw").parent().show();
                                    }else {
                                        $("#list-draw").parent().hide();
                                    }
                                }
                            }
                        },
                        error:function (XMLHttpRequest,textStatus,errorThorwn) {
                            console.error(XMLHttpRequest);
                            console.error(textStatus);
                            console.error(errorThorwn);
                        },
                        complete:function (XMLHttpRequest,textStatus) {}
                    });
                }else {
                    jeBox.alert("您无权限进行此操作");
                }
            }
        },"json");
    });
    $("a[data-drawer='fullScreen']").click(function(){
        var docElm = document.getElementsByTagName("html")[0];
        if (docElm.requestFullscreen) {
            docElm.requestFullscreen();
        }
        else if (docElm.msRequestFullscreen) {
            jeBox.alert("浏览器不支持全屏API或已被禁用");
        }
        else if (docElm.mozRequestFullScreen) {
            docElm.mozRequestFullScreen();
        }
        else if (docElm.webkitRequestFullScreen) {
            docElm.webkitRequestFullScreen();
        }else{
            jeBox.alert("浏览器不支持全屏API或已被禁用");
        }
    });
    //系统帮助
    $('.qiteye').click(function() {
        window.open("/help/help-cn.pdf");
    });
    //系统帮助
    $(".hlepsure").click(function() {
        window.open("/help/help-cn.pdf");
    })
    $(".logout-btn").click(function () {
        $.ajax({
            url: "/user/logout.do",
            method: "POST",
            data: {},
            dataType:"json",
            beforeSend:function(XMLHttpRequest){},
            success:function(data,textStatus,XMLHttpRequest){
                if(data.success = "1"){
                    window.location = "/login.html";
                }else if(data.success = "0"){
                    jeBox.alert("注销不成功");
                }
            },
            error:function (XMLHttpRequest,textStatus,errorThorwn) {
                console.error(XMLHttpRequest);
                console.error(textStatus);
                console.error(errorThorwn);
            },
            complete:function (XMLHttpRequest,textStatus) {}
        })
    });
    /*----------刷新页面功能------------*/
    $(document).on("click","#refreChTab",function(){
        if(
            header.page != "observationPointkpi" &&
            header.page != "userSidekpi" &&
            header.page != "serverSidekpi" &&
            header.page != "httpSerkpi" &&
            header.page != "oracleSerkpi" &&
            header.page != "mysqlSerkpi" &&
            header.page != "sqlSerkpi" &&
            header.page != "urlkpi" &&
            header.page != "heatMap" &&
            header.page != "baowenJy" &&
            header.page != "netCockpit" &&
            header.page != "systemCapital" &&
            header.page != "cockpit"
        ){
            location.reload();
        }
    });
    /**
     * 将刷新粒度赋值
     */
    $.ajax({
        url:"/watchpointController/getUserConfigureBeanByKey.do",
        method:"POST",
        async:false,
        data:{
            key:"dataRefreshTime"
        },
        dataType:"json",
        beforeSend:function (XMLHttpRequest) {},
        success:function (data,textStatus,XMLHttpRequest) {
            var timelidu = $('input[name="timelidu"]');
            switch (data){
                case 1:
                    timelidu.eq(1).attr("checked","checked");
                    break;
                case 10:
                    timelidu.eq(2).attr("checked","checked");
                    break;
                case 30:
                    timelidu.eq(3).attr("checked","checked");
                    break;
                case 60:
                    timelidu.eq(4).attr("checked","checked");
                    break;
                default:
                    timelidu.eq(1).attr("checked","checked");
            }
        },
        error:function (XMLHttpRequest,textStatus,errorThorwn) {
            $('input[name="timelidu"]').eq(1).attr("checked","checked");
            console.error(XMLHttpRequest);
            console.error(textStatus);
            console.error(errorThorwn);
        },
        complete:function (XMLHttpRequest,textStatus) {}
    });
    /**
     * 展示授权是否过期
     * 604800 小于一周给提示
     */
    $.ajax({
        url:"/systemSet/readProductLicensSet.do",
        method:"POST",
        data:{},
        dataType:"json",
        beforeSend:function (XMLHttpRequest) {},
        success:function (data,textStatus,XMLHttpRequest) {
            if($.myTime.DateToUnix(data.validterm+' 00:00:00') - $.myTime.CurTime() <= 604800){
                $("#licenseBox").text("有授权许可即将到期").click(function () {
                    location.href = '/settingindex.html';
                })
            }
        },
        error:function (XMLHttpRequest,textStatus,errorThorwn) {
            console.error(XMLHttpRequest);
            console.error(textStatus);
            console.error(errorThorwn);
        },
        complete:function (XMLHttpRequest,textStatus) {}
    });
});
